package application;

import java.util.ArrayList;
import java.util.List;

public class Tehe {

	private double value;
	private String operation;
	private double lastvalue;
	private int domath;

	private boolean calculated;

	private List<Double> calcans = new ArrayList<>();

	/**
	 * Gets value
	 * 
	 * @return
	 */

	public double getValue() {
		return value;
	}

	/**
	 * Gets lastvalue variable
	 * 
	 * @return
	 */
	public double getLastvalue() {
		return lastvalue;
	}

	/**
	 * cleans operation, lastvalue, value, calculated boolean
	 */
	public void resetCalc() {
		operation = null;
		lastvalue = 0;
		value = 0;
		domath = 0;
		calculated = false;

	};

	/**
	 * Sets operation mark
	 * 
	 * @param Operation
	 */
	public void setOperation(String Operation) {

		operation = Operation;

	}

	/**
	 * Set value to number
	 * 
	 * @param Number
	 */
	public void setValue(double Number) {
		value = Number;
	}

	/**
	 * Sets lastvalue from value
	 */
	public void setLastValue() {
		lastvalue = value;
		value = 0;

	}

	/**
	 * Sets domath to 1
	 */

	public void setDoMath() {
		domath = 1;
	}

	/**
	 * Do basic calculations
	 * 
	 * @return answer (lastvalue)
	 */
	private double calculate() {
		double radians = 0;
		switch (operation) {
		case "+":
			lastvalue = lastvalue + value;
			break;
		case "-":
			lastvalue = lastvalue - value;
			break;
		case "*":
			lastvalue = lastvalue * value;
			break;
		case "/":
			if (value != 0)
				lastvalue = lastvalue / value;
			break;
		case "^":
			lastvalue = Math.pow(lastvalue, value);
			break;
		case "perc":
			lastvalue = value * (lastvalue / 100);

			break;
		case "sin":
			radians = Math.toRadians(value);
			lastvalue = Math.sin(radians);
			break;
		case "cos":
			radians = Math.toRadians(value);
			lastvalue = Math.cos(radians);
			break;
		case "tan":
			radians = Math.toRadians(value);
			lastvalue = Math.tan(radians);
			break;
		case "cot":

			radians = Math.toRadians(value);
			lastvalue = 1.0 / Math.tan(radians);
			break;
		default:
			break;
		}

		calculated = true;
		value = 0;
		calcans.add(lastvalue);
		operation = null;
		domath = 0;

		return lastvalue;

	}

	/**
	 * Gets Calculation answers history
	 * 
	 * @return - list<double> with calculation history
	 */

	public List<Double> getcalculationhistory() {
		return calcans;
	}

	/**
	 * Function with math logics, based on operator, value and lastvalue
	 * 
	 * @param number
	 *            - Input number to do math with
	 * @return Calculation answer
	 */
	public double getAnswer(double number) {
		// boolean contains = false;

		if (domath != 0 && lastvalue != 0 && operation != null && calculated == true) {
			value = number;
			return calculate();
		} else if (domath != 0 && operation != null) {
			setLastValue();
			value = number;
			return calculate();
		} else if (operation == null && domath == 0) {
			value = number;
			return value;
		}

		return value;

	}
};
