package application;

import java.io.File;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import javax.swing.JOptionPane;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;

public class Kalkulaator extends Application {

	private String var = "0";
	private double result;
	private String calcvar = "0";
	private boolean deccalc = false;
	private boolean okpressed = false;
	private boolean trigfunc = false;
	private boolean actionclicked = false;
	private Label lblinfo = new Label("");
	private Label lblcalculation = new Label("");

	NumberFormat nf = new DecimalFormat("##.######");

	Tehe tehe = new Tehe();

	@Override
	public void start(Stage primaryStage) {

		Button btn1 = new Button();
		Button btn2 = new Button();
		Button btn3 = new Button();
		Button btn4 = new Button();
		Button btn5 = new Button();
		Button btn6 = new Button();
		Button btn7 = new Button();
		Button btn8 = new Button();
		Button btn9 = new Button();
		Button btn0 = new Button();
		Button btndec = new Button();
		Button btnplus = new Button();
		Button btnminus = new Button();
		Button btnmulti = new Button();
		Button btndiv = new Button();
		Button btnpow = new Button();
		Button btnsin = new Button();
		Button btncos = new Button();
		Button btntan = new Button();
		Button btncot = new Button();
		Button btnperc = new Button();
		Button btnclear = new Button();
		Button btnce = new Button();
		Button btnok = new Button();
		Button btnhistory = new Button();

		btn1.setText("1");
		btn2.setText("2");
		btn3.setText("3");
		btn4.setText("4");
		btn5.setText("5");
		btn6.setText("6");
		btn7.setText("7");
		btn8.setText("8");
		btn9.setText("9");
		btn0.setText("0");
		btndec.setText(".");
		btnplus.setText("+");
		btnminus.setText("-");
		btnmulti.setText("*");
		btndiv.setText("/");
		btnpow.setText("^");
		btnperc.setText("%");
		btnsin.setText("sin");
		btncos.setText("cos");
		btntan.setText("tan");
		btncot.setText("cot");
		btnclear.setText("C");
		btnce.setText("Ce");
		btnok.setText("=");
		btnhistory.setText("Calculated history");

		lblcalculation.textProperty().setValue("");

		lblinfo.textProperty().setValue("0");

		// btn1 eventhandler
		btn1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setNumber("1");
			}
		});

		// btn2 eventhandler
		btn2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				setNumber("2");
			}
		});
		// btn3 eventhandler
		btn3.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setNumber("3");
			}
		});
		// btn4 eventhandler
		btn4.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setNumber("4");
			}
		});
		// btn5 eventhandler
		btn5.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				setNumber("5");
			}
		});
		// btn6 eventhandler
		btn6.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setNumber("6");
			}
		});
		// btn7 eventhandler
		btn7.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setNumber("7");
			}
		});
		// btn8 eventhandler
		btn8.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setNumber("8");
			}
		});
		// btn9 eventhandler
		btn9.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setNumber("9");
			}
		});
		// btn0 eventhandler
		btn0.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				setNumber("0");
			}
		});
		// btndec decimalplace eventhandler
		btndec.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				clickDec();
			}
		});

		// btnplus - plus button eventhandler
		btnplus.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickAction("+");
			}
		});
		// btnminus - minus button eventhandler
		btnminus.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickAction("-");
			}
		});
		// btnmulti - multiply button eventhandler
		btnmulti.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickAction("*");

			}
		});
		// btndiv - divide button eventhandler
		btndiv.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickAction("/");

			}
		});
		// btnpow - divide button eventhandler
		btnpow.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickAction("^");

			}
		});
		// btnsin - sinus button eventhandler
		btnsin.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickTrig("sin");
			}
		});
		// btntan - tangens button eventhandler
		btntan.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickTrig("tan");

			}
		});
		// btncos - cosinus button eventhandler
		btncos.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickTrig("cos");

			}
		});
		// btncot - cotangens button eventhandler
		btncot.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickTrig("cot");

			}
		});
		// btnperc - percentage button eventhandler
		btnperc.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickAction("%");

			}
		});
		// btnok - equals/calculate button eventhandler
		btnok.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickOk();
			}
		});
		// btnclear - clear button eventhandler
		btnclear.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickClear();

			}
		});
		// btnce - clear current value eventhandler
		btnce.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				clickCe();

			}
		});

		btnhistory.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				List<Double> calculationhistory = tehe.getcalculationhistory();

				String message = "\n History of calculation \n ";

				for (Double calc : calculationhistory) {
					message += "\n" + "Calculation:" + calc;
				}
				JOptionPane.showMessageDialog(null, message);

			}
		});

		StackPane root = new StackPane();

		VBox vb = new VBox(15);
		vb.getStyleClass().add("vbox");
		vb.setAlignment(Pos.CENTER);

		HBox tophb = new HBox();
		tophb.getStyleClass().add("hbox_label");
		tophb.setSpacing(8);
		tophb.getChildren().add(lblcalculation);

		HBox hb = new HBox();
		hb.getStyleClass().add("hbox_label");
		hb.setSpacing(8);
		hb.getChildren().add(lblinfo);

		HBox hb1 = new HBox();
		hb1.getStyleClass().add("hbox");
		hb1.setSpacing(8);
		hb1.getChildren().add(btn7);
		hb1.getChildren().add(btn8);
		hb1.getChildren().add(btn9);
		hb1.getChildren().add(btnminus);
		hb1.getChildren().add(btnsin);
		hb1.getChildren().add(btncos);

		HBox hb2 = new HBox();
		hb2.getStyleClass().add("hbox");
		hb2.setSpacing(8);
		hb2.getChildren().add(btn4);
		hb2.getChildren().add(btn5);
		hb2.getChildren().add(btn6);
		hb2.getChildren().add(btnplus);
		hb2.getChildren().add(btntan);
		hb2.getChildren().add(btncot);

		HBox hb3 = new HBox();
		hb3.getStyleClass().add("hbox");
		hb3.setSpacing(8);
		hb3.getChildren().add(btn1);
		hb3.getChildren().add(btn2);
		hb3.getChildren().add(btn3);
		hb3.getChildren().add(btnmulti);
		hb3.getChildren().add(btnpow);
		hb3.getChildren().add(btnperc);

		HBox hb4 = new HBox();
		hb4.getStyleClass().add("hbox");
		hb4.setSpacing(8);
		hb4.getChildren().add(btn0);
		hb4.getChildren().add(btndec);
		hb4.getChildren().add(btnok);
		hb4.getChildren().add(btnclear);
		hb4.getChildren().add(btnce);
		hb4.getChildren().add(btndiv);

		HBox hb5 = new HBox();
		hb5.getStyleClass().add("hbox_history");
		hb5.setSpacing(8);
		hb5.getChildren().add(btnhistory);

		vb.getChildren().addAll(tophb, hb, hb1, hb2, hb3, hb4, hb5);

		lblcalculation.setStyle("-fx-font-size:16;");

		root.getChildren().add(vb);

		Scene scene = new Scene(root, 400, 450);
		try {
			scene.getStylesheets().add((new File("src/application/style.css")).toURI().toURL().toExternalForm());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent ke) {
				String code = ke.getCode().toString();
				if (code.contains("DIGIT") || code.contains("NUMPAD")) {
					setNumber(code.substring(code.length() - 1));
				}
				switch (code) {
				case "PLUS":
					clickAction("+");
					;
					break;
				case "ADD":
					clickAction("+");
					;
					break;
				case "MINUS":
					clickAction("-");
					;
					break;
				case "SUBTRACT":
					clickAction("-");
					;
					break;
				case "DIVIDE":
					clickAction("/");
					;
					break;
				case "MULTIPLY":
					clickAction("*");
					break;
				case "DECIMAL":
					clickDec();
					break;
				case "ENTER":
					clickOk();
					break;
				case "DELETE":
					clickClear();
					break;
				case "BACK_SPACE":
					clickCe();
					break;
				default:
					break;
				}
			}
		});

		primaryStage.setTitle("Kalkulaator v0.3beta");
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Sets number to var
	 * 
	 * @param number
	 *            - Input number
	 */

	public void setNumber(String number) {

		if (number != null) {

			if (var == "0") {
				lblinfo.textProperty().setValue(number);
				var = lblinfo.textProperty().getValue();
			} else {
				lblinfo.textProperty().setValue(var + number);
				var = lblinfo.textProperty().getValue();

			}

		} else {
			var = lblinfo.textProperty().getValue();
			if (Integer.parseInt(lblinfo.textProperty().getValue()) != 0) {
				lblinfo.textProperty().setValue(var + "0");
				var = lblinfo.textProperty().getValue();
			}
		}

	}

	/**
	 * Clears currently displayed value
	 */
	public void clickCe() {
		if (okpressed) {
			clickClear();
		} else {
			lblinfo.textProperty().setValue("0");
			var = "";
			deccalc = false;
		}
	}

	/**
	 * Resets all calculator variables
	 */
	public void clickClear() {
		tehe.resetCalc();
		var = "0";
		lblinfo.textProperty().setValue(var);
		lblcalculation.textProperty().setValue("");
	}

	/**
	 * Initiate calculation to get answer
	 */
	public void clickOk() {
		if (var != null && okpressed == false) {
			var = lblinfo.textProperty().getValue();

			result = Double.valueOf((tehe.getAnswer(Double.parseDouble(var))));
			nf.format(result);
			lblinfo.textProperty().setValue(Double.toString(result));

			calcvar = lblcalculation.textProperty().getValue();
			calcvar = calcvar + var + " = ";
			lblcalculation.textProperty().setValue(calcvar);

			tehe.setDoMath();
			okpressed = true;
			actionclicked = false;
			deccalc = false;

		}
	}

	/**
	 * Does trigonometric calculations (sin, cos, tan, cot)
	 * 
	 * @param symbol
	 *            - input calculation type (sin, cos, tan, cot)
	 */

	public void clickTrig(String symbol) {
		if (var != null) {
			tehe.setOperation(symbol);
			tehe.setDoMath();
			var = lblinfo.textProperty().getValue();

			result = Double.valueOf((tehe.getAnswer(Double.parseDouble(var))));
			nf.format(result);
			lblinfo.textProperty().setValue(Double.toString(result));
			lblcalculation.textProperty().setValue(symbol + "(" + var + ") = " + result);
			trigfunc = true;
			var = "0";
			deccalc = false;
			okpressed = false;
		}
	}

	/**
	 * Sets action and calculates answer if nesccesary
	 * 
	 * @param symbol
	 *            - input action type (+, -, *, /, ^, %)
	 */
	public void clickAction(String symbol) {
		if (var != null) {
			var = lblinfo.textProperty().getValue();
			result = Double.valueOf((tehe.getAnswer(Double.parseDouble(var))));
			nf.format(result);
			lblinfo.textProperty().setValue(Double.toString(result));

			if (okpressed || trigfunc) {
				lblcalculation.textProperty().setValue(var + " " + symbol + " ");
			} else if (actionclicked) {
				lblcalculation.textProperty().setValue(result + " " + symbol);
			}

			else {
				calcvar = lblcalculation.textProperty().getValue();
				calcvar = calcvar + result + " " + symbol + " ";
				lblcalculation.textProperty().setValue(calcvar);
			}
			tehe.setOperation(symbol);
			var = "0";
			tehe.setDoMath();
			deccalc = false;
			trigfunc = false;
			okpressed = false;
			actionclicked = true;

		}
	}

	/**
	 * Initates decimal place calculation
	 */
	public void clickDec() {
		if (deccalc == false && okpressed == false) {
			if (lblinfo.textProperty().getValue() != null) {
				lblinfo.textProperty().setValue(var + ".");
				var = lblinfo.textProperty().getValue();
				deccalc = true;
			} else {
				lblinfo.textProperty().setValue("0.");
				deccalc = true;
				var = lblinfo.textProperty().getValue();

			}

		}
	}

}
